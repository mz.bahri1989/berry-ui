# BerryUI, another UI Framework!

This a small UI component to make things easy with React. For now, it includes input field, toggle switch and alert modal.

Check out [this repo](https://gitlab.com/mz.bahri1989/blueberry-ui-example) as an example.