const path = require('path');
module.exports = {
    mode: "production",
    entry: {
        index:['./src/index.js']
    },
    output: {
        filename: "[name].js",
        path: path.join(__dirname, "/"),
        publicPath: '/',
        libraryTarget: 'commonjs2'
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            components: path.resolve(__dirname, '..', 'src/components'),
        }
    },
    externals: {
        // Don't bundle react or react-dom
        react: {
            commonjs: "react",
            commonjs2: "react",
            amd: "React",
            root: "React"
        },
        "react-dom": {
            commonjs: "react-dom",
            commonjs2: "react-dom",
            amd: "ReactDOM",
            root: "ReactDOM"
        }
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                use: [{
                    loader:'babel-loader',
                    options: {
                        presets: ["@babel/preset-env",'@babel/preset-react'],
                        plugins: [
                            '@babel/plugin-proposal-object-rest-spread',
                            "@babel/plugin-transform-runtime"
                        ]
                    }

                }],
                exclude: /node_modules/,
                //include:/node_modules\/berry-ui/,

            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader', {
                    loader: "postcss-loader",
                    options: {
                        plugins: () => [require("autoprefixer")({ grid: true }),
                            require('cssnano')({preset: 'default'})
                        ],
                        minimize: true
                    },
                }, {
                        loader: 'sass-loader',
                        options: {
                            data: `
                            @import "src/css/_variables";`
                        }
                    }]
            },
            {
                test: /\.(ttf|eot|otf|svg|png|jpg)$/,
                loader: 'file-loader'
            },
            {
                test: /\.(woff|woff2)$/,
                loader: 'url-loader'
            }
        ],

    }
};