import {AlertComponent} from "./js/Components/AlertComponent";
import {InputComponent} from "./js/Components/InputComponent";
import {ToggleSwitchComponent} from "./js/Components/ToggleSwitchComponent";

export {
    AlertComponent,
    InputComponent,
    ToggleSwitchComponent
}
