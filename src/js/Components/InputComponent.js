import React, {Fragment} from "react";
import "../../css/_input.scss";

const InputComponent = (props)=>{
        const {type,value,placeholder,label,id,onChangeAction,className} = props;
       return(<Fragment>
               {label ? <label htmlFor={id || undefined}>{label}</label>:''}
               <input type={type || 'text'}
                      value={value || ''}
                      id={id || undefined}
                      placeholder={placeholder || undefined}
                      onChange={(e) => onChangeAction(e)}
                      className={`berry-input__field ${className || ''}`}/>
       </Fragment>

       )

};

export {InputComponent};