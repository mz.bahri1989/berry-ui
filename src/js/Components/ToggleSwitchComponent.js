import React from "react";
import "../../css/_toggle_switch.scss";

class ToggleSwitchComponent extends React.Component {
    constructor(props) {
        super(props);
        this.toggleHandler = this.toggleHandler.bind(this);
        this.state = {
            active: false
        }
    }

    toggleHandler(e){
        this.setState({active:!this.state.active});

        //in the end, run the callback passed from the parent.
        //Useful for dispatching actions to redux store.
        this.props.onClick(e)
    }

    render() {
        const id = this.props.id || undefined;
        return (
            <div className="berry-toggle__wrapper">
                <label htmlFor={id} className="berry-toggle__label">
                    {this.props.label}
                </label>
                <span
                    onClick={this.toggleHandler}
                    className={`berry-toggle__switch ${this.state.active ? 'active':'disabled'} ${this.props.className || ''}`}>
                    <input
                        type="radio"
                        id={id}
                        defaultChecked={this.state.active}
                    />
                </span>
            </div>
        )
    }
}

export {ToggleSwitchComponent};