import React from "react";
import "../../css/_alert.scss";

class AlertComponent extends React.Component {
    constructor(props) {
        super(props);
        this.closeModal = this.closeModal.bind(this);
        this.state = {
            show: false
        }
    }
    componentDidMount(){
        this.setState({show: true})
    }

    closeModal(e) {
        // blah blah blah
        this.setState({show:false});
        // This could be used for marketing purposes.
        // For now it  just unmounts the component
        this.props.closeAction && this.props.closeAction(e)
    }

    render() {
        return this.state.show ? <div className={`berry__alert__${this.props.type} ${this.props.className || ''}`}>
            <div className={`berry__alert__${this.props.type}__content`}>
                <button
                    type="button"
                    className="berry__alert--close"
                    onClick={this.closeModal}
                >+
                </button>
                {this.props.children}
            </div>
        </div>:''
    }
}

export {AlertComponent};